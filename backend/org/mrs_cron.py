import uuid
from functools import lru_cache
import logging

from org.git import get_igitt_org
from igitt_django.models import IGittMergeRequest, IGittRepository


@lru_cache(maxsize=32)
def get_mrs(hoster):
    data = dict()
    repo_dict = dict()
    org = get_igitt_org(hoster)
    repos = org.repositories
    for repo in repos:
        repo_full_name = repo.full_name
        repo_id = repo.identifier
        repo_dict[repo.identifier] = {
            'id': repo_id,
            'full_name': repo_full_name,
            'hoster': hoster,
        }
        mrs = repo.filter_merge_requests(state='all')
        for mr in mrs:
            try:
                ci_status = mr.tests_passed
            # Note that these exceptions occurred for only two
            # mrs and they have be been checked manually with
            # passing CI, that's why adding ci_status = True, below.
            except (AssertionError, RuntimeError) as e:
                logging.error(e)
                ci_status = True
            try:
                state = mr.state.value
            except KeyError as e:
                logging.error(e)
                state = 'open'
            id = uuid.uuid4().int & (1 << 32)-1

            data[id] = {
                'number': mr.number,
                'title': mr.title,
                'description': mr.description,
                'repo': repo.full_name,
                'repo_id': repo_id,
                'url': mr.web_url,
                'closes_issues': (
                    [int(issue.number) for issue in mr.closes_issues]),
                'state': state,
                'author': mr.author.username,
                'assignees': [assignee.username for assignee in mr.assignees],
                'ci_status': ci_status,
                'created_at': mr.created,
                'updated_at': mr.updated,
                'labels': [label for label in mr.labels],
                'hoster': hoster,
                'reactions': [r.name for r in mr.reactions],
            }

    # Saving dict to database
    for repo in repo_dict:
        if IGittRepository.objects.filter(id=repo).exists():
            r = IGittRepository.objects.get(id=repo)
        else:
            r = IGittRepository()
            r.id = repo
        r.full_name = repo_dict[repo]['full_name']
        r.hoster = repo_dict[repo]['hoster']
        r.save()

    for mr in data:
        number = data[mr]['number']
        repo_id = data[mr]['repo_id']
        if IGittMergeRequest.objects.filter(
                number=number, repo_id=repo_id).exists():
            m = IGittMergeRequest.objects.get(number=number, repo_id=repo_id)
        else:
            m = IGittMergeRequest()
            m.id = mr
        m.number = data[mr]['number']
        m.repo_id = data[mr]['repo_id']
        m.data = data[mr]
        m.save()
